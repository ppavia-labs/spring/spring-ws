package ppa.spring.springwsinit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWsInitApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWsInitApplication.class, args);
    }

}
